package model;
// Generated 27/09/2015 11:19:22 by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * CursoComponente generated by hbm2java
 */
@Entity
@SequenceGenerator(name = "cursocomponente_seq", sequenceName = "cursocomponente_seq_no_banco", allocationSize = 1, initialValue = 1) 
@Table(name = "curso_componente", schema = "public"
)
public class CursoComponente implements java.io.Serializable {

    private int id;
    private ComponenteCurricular componenteCurricular;
    private Curso curso;
    private Boolean obrigatorio;
    private Integer semestre;

    public CursoComponente() {
    }

    public CursoComponente(int id, ComponenteCurricular componenteCurricular, Curso curso) {
        this.id = id;
        this.componenteCurricular = componenteCurricular;
        this.curso = curso;
    }

    public CursoComponente(int id, ComponenteCurricular componenteCurricular, Curso curso, Boolean obrigatorio, Integer semestre) {
        this.id = id;
        this.componenteCurricular = componenteCurricular;
        this.curso = curso;
        this.obrigatorio = obrigatorio;
        this.semestre = semestre;
    }

    @Id
    @GeneratedValue(generator = "cursocomponente_seq")
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "componente_curricular_idcomponentecurricular", nullable = false)
    public ComponenteCurricular getComponenteCurricular() {
        return this.componenteCurricular;
    }

    public void setComponenteCurricular(ComponenteCurricular componenteCurricular) {
        this.componenteCurricular = componenteCurricular;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "curso_idcurso", nullable = false)
    public Curso getCurso() {
        return this.curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Column(name = "obrigatorio")
    public Boolean getObrigatorio() {
        return this.obrigatorio;
    }

    public void setObrigatorio(Boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }

    @Column(name = "semestre")
    public Integer getSemestre() {
        return this.semestre;
    }

    public void setSemestre(Integer semestre) {
        this.semestre = semestre;
    }

}
