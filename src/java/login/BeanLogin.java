
package login;

import java.io.Serializable;

/**
 *
 * @author Neto
 */
public class BeanLogin implements Serializable {
    /**
     * private String msg;
    private Docente docente;
    private boolean admin;

    public LoginMBean() {
        this.docente = new Docente();
    }

    //validate login
    public String login() {
        boolean valid = new EntityDao().isDocente(this.getDocente());

        if (valid) {
            this.setDocente(new EntityDao().getToLogin(getDocente()));
            //GAMBI
            if (this.docente.getEmail().equals("admin@admin.com")) {
                this.setAdmin(true);
            }
            HttpSession session = SessionBean.getSession();
            session.setAttribute("nome", this.getDocente().getNome());
            return "admin";
        } else {
            this.docente = new Docente();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Os dados informados não correspondem aos cadastrados.",
                            "Please enter correct email and Password"));
            return "index";
        }
    }

    //logout event, invalidate session
    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        return "index";
    }

    public boolean isLogged() {
        return new EntityDao().isDocente(this.getDocente());
    }

    //get set
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the docente
     
    public Docente getDocente() {
        return docente;
    }

    /**
     * @param docente the docente to set
     
    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    /**
     * @return the admin
     
    public boolean isAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
     */
}
