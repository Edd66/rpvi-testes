package validacao.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("validacao.LinkLattesValidator")
public class LinkLattesValidator implements Validator {

    private final String nome = "http://lattes.cnpq.br/" + "[0-9]+";
    private Pattern pattern;
    private Matcher matcher;

    public LinkLattesValidator() {
        pattern = Pattern.compile(nome);
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {
        matcher = pattern.matcher(value.toString());

        if (!matcher.matches()) {

            FacesMessage msg
                    = new FacesMessage("Link validation failed.",
                            "Link inválido, por favor siga o padrão: http://lattes.cnpq.br/999999999999999");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);

        } else if (value.toString().length() > 155) {
            FacesMessage msg
                    = new FacesMessage("Link validation failed.",
                            "Excedeu o limite máximo de caracteres que é 155.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
}
